# File: /Makefile
# Project: keycloak-custom-from-provider
# File Created: 28-01-2022 12:35:10
# Author: Clay Risser
# -----
# Last Modified: 25-02-2022 13:28:32
# Modified By: Clay Risser
# -----
# BitSpur Inc (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include mkpm.mk
ifneq (,$(MKPM_READY))
include $(MKPM)/gnu
include $(MKPM)/mkchain

MVN ?= mvn

export POM_ARTIFACT_ID ?= $(shell $(CAT) pom.xml | \
	$(GREP) "<artifactId>" | \
	$(HEAD) -n1 | \
	$(SED) 's|^\s*<artifactId>||g' | \
	$(SED) 's|</artifactId>||g')
export POM_VERSION ?= $(shell $(CAT) pom.xml | \
	$(GREP) "<version>" | \
	$(HEAD) -n1 | \
	$(SED) 's|^\s*<version>||g' | \
	$(SED) 's|</version>||g')

.PHONY: install
install:
	@$(MVN) clean install

.PHONY: build
build: install
	@$(MVN) package

.PHONY: clean
clean: ##
	-@$(GIT) clean -fXd \
		$(MKPM_GIT_CLEAN_FLAGS) \
		$(NOFAIL)

.PHONY: purge
purge: clean ##
	@$(GIT) clean -fXd

.PHONY: docker-%
docker-%:
	@$(MAKE) -sC docker $(subst docker-,,$@) ARGS=$(ARGS)

-include $(call actions,$(ACTIONS))

endif
