package online.promanager.keycloak.provider;

import javax.ws.rs.core.MultivaluedMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.auto.service.AutoService;

import org.jboss.logging.Logger;
import org.keycloak.Config;
import org.keycloak.authentication.FormAction;
import org.keycloak.authentication.FormActionFactory;
import org.keycloak.authentication.FormContext;
import org.keycloak.authentication.forms.RegistrationProfile;
import org.keycloak.events.Details;
import org.keycloak.events.Errors;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.UserModel;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.userprofile.UserProfile;
import org.keycloak.userprofile.UserProfileContext;
import org.keycloak.userprofile.UserProfileProvider;
import org.keycloak.userprofile.ValidationException;

import twitter4j.JSONObject;

import org.keycloak.services.messages.Messages;
import org.keycloak.services.validation.Validation;

@AutoService(FormActionFactory.class)
public class EmailFormActionFactory extends RegistrationProfile {
    private static final Logger log = Logger.getLogger(EmailFormActionFactory.class);

    @Override
    public void validate(org.keycloak.authentication.ValidationContext context) {
        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        context.getEvent().detail(Details.REGISTER_METHOD, "form");
        UserProfileProvider profileProvider = context.getSession().getProvider(UserProfileProvider.class);
        UserProfile profile = profileProvider.create(UserProfileContext.REGISTRATION_PROFILE, formData);
        try {
            assertNoRemoteAccountExists(formData.get("username").get(0), formData.get("email").get(0));
            profile.validate();
        } catch (ValidationException pve) {
            List<FormMessage> errors = Validation.getFormErrorsFromValidation(pve.getErrors());
            if (pve.hasError(Messages.EMAIL_EXISTS, Messages.INVALID_EMAIL)) {
                context.getEvent().detail(Details.EMAIL, profile.getAttributes().getFirstValue(UserModel.EMAIL));
            }
            if (pve.hasError(Messages.EMAIL_EXISTS)) {
                context.error(Errors.EMAIL_IN_USE);
            } else {
                context.error(Errors.INVALID_REGISTRATION);
            }
            context.validationError(formData, errors);
            return;
        } catch (Exception e) {
            List<FormMessage> errors = new ArrayList<FormMessage>();
            errors.add(new FormMessage(e.getMessage()));
            context.validationError(formData, errors);
            return;
        }
        context.success();
    }

    private void assertNoRemoteAccountExists(String username, String email) throws Exception {
        String accountSyncHost = "http://localhost:3000";
        try {
            URL url = new URL(accountSyncHost + "/sync/" + username);
            // JSONObject data = new JSONObject();
            // data.put("hello", "world");
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setDoOutput(true);
            httpConnection.setInstanceFollowRedirects(true);
            httpConnection.setRequestMethod("GET");
            // httpConnection.setRequestProperty("Content-Type", "application/json");
            httpConnection.setRequestProperty("Accept", "application/json");
            // DataOutputStream wr = new DataOutputStream(httpConnection.getOutputStream());
            // wr.write(data.toString().getBytes());
            Integer responseCode = httpConnection.getResponseCode();
            BufferedReader bufferedReader;
            if (responseCode > 199 && responseCode < 300) {
                bufferedReader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            } else {
                bufferedReader = new BufferedReader(new InputStreamReader(httpConnection.getErrorStream()));
            }
            StringBuilder content = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line).append("\n");
            }
            bufferedReader.close();
            JSONObject response = new JSONObject(content.toString());
            System.out.println("code " + responseCode);
            System.out.println(response);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new Exception("sync server " + accountSyncHost + " is down");
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("sync server " + accountSyncHost + " is down");
        }
    }

    @Override
    public void buildPage(FormContext context, LoginFormsProvider form) {
        super.buildPage(context, form);
    }

    @Override
    public void success(FormContext context) {
        UserModel user = context.getUser();
        UserProfileProvider profileProvider = context.getSession().getProvider(UserProfileProvider.class);
        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        profileProvider.create(
                UserProfileContext.REGISTRATION_PROFILE,
                formData,
                user).update();
        user.setAttribute("hello", formData.get("hello"));
    }

    @Override
    public String getHelpText() {
        return super.getHelpText();
    }

    @Override
    public void init(Config.Scope config) {
        super.init(config);
    }

    @Override
    public void close() {
        super.close();
    }

    @Override
    public FormAction create(KeycloakSession session) {
        return this;
    }
}
